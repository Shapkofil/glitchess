extends ColorRect


export(float) var amplitute = 4.0
export(float) var time = 0.7

onready var board_node = get_parent().get_parent().get_parent().get_node("Board")

func _ready():
	board_node.connect("glitched", self, "_on_Glitched")
	print(board_node.board)
	pass


func _on_Glitched(a, b):
	self.get_material().set_shader_param("AMPLITUDE", amplitute)
	yield(get_tree().create_timer(time), "timeout")
	print(self.material.get_shader_param("AMPLITUDE"))
	self.get_material().set_shader_param("AMPLITUDE", 0.0)
	pass
