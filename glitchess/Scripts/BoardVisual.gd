extends TileMap

onready var board_node = get_parent()
onready var input_agent = board_node.get_node("InputAgent")
onready var tile_indecies=tile_set.get_tiles_ids()

var move_query_dict = {"-":0,"0":1,"1":2}

var active = false

func _ready():
	board_node.connect("moves_queried", self, "on_Moves_Queried")
	board_node.connect("visual_reset", self, "on_Visual_Reset")
	pass

func on_Moves_Queried(move_set, capture_set, glitch_set):
	active = true
	for pos in move_set:
		set_cellv(Vector2(7-pos.y,pos.x)-Vector2(4,4),tile_indecies[1])
	for pos in capture_set:
		set_cellv(Vector2(7-pos.y,pos.x)-Vector2(4,4),tile_indecies[2])
	for pos in glitch_set:
		set_cellv(Vector2(7-pos.y,pos.x)-Vector2(4,4),tile_indecies[3])

func on_Visual_Reset():
	if not active:
		return
	active = false
	for x in range(8):
		for y in range(8):
			set_cellv(Vector2(7-x,y)-Vector2(4,4),tile_indecies[0])
