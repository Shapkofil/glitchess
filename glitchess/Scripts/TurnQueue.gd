extends CanvasLayer

class_name TurnQueue

var active_character
var acid= 0

onready var players = get_children()
onready var board_node = get_parent().get_parent().get_node("Board")
onready var board_shadow = board_node.get_parent().get_node("Shadow")
onready var title_screen = get_parent().get_node("Title Screen").get_node("Title Screen")
onready var end_screen = get_parent().get_node("End Screen").get_node("End Screen")
onready var glitch_list = title_screen.get_node("Glitches")

signal queue_changed(acid)

func update_game_state():
	var display = end_screen.get_node("Display")
	var end_message = ""
	print(board_node.game_state)
	match board_node.game_state:
		"checkmate":
			end_message = "WHITE WINS"
			if acid == 0:
				end_message = "BLACK WINS"
			display.text = "\n\n\n\n\n%s"%end_message
			trigger_end_screen()

		"glitchmate":
			end_message = "WHITE WINS\nWITH HACKS"
			if acid == 0:
				end_message = "BLACK WINS\nWITHHACK"
			display.text = "\n\n\n\n\n%s"%end_message
			trigger_end_screen()

		"stalemate":
			end_message = "DRAW"
			display.text = "\n\n\n\n\n%s"%end_message
			trigger_end_screen()

func _ready():
	board_node.initiate()
	while true:
		emit_signal("queue_changed", acid)
		update_game_state()
		yield(play_turn(),"completed")
		# Check for check-mate

func play_turn():
	active_character = players[acid]
	acid = (acid + 1) % players.size()
	yield(active_character.play_turn(),"completed")

func trigger_end_screen():
	for player in players:
		player.visible = false
	board_node.visible = false
	board_shadow.visible = false

	end_screen.visible = true


func _on_Play_pressed():

	for player in players:
		player.visible = true
	board_node.visible = true
	board_shadow.visible = true
	title_screen.visible = false
	end_screen.visible = false

	board_node.initiate()


func _on_Exit_pressed():
	get_tree().quit()


func _on_Glitches_pressed():
	glitch_list.visible = not glitch_list.visible
