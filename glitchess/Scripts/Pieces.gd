extends Node2D

# Called when the node enters the scene tree for the first time.

export (Texture) var atlas
var pieces = []
var textures
var input_agent
var board_node
var board_data
var board_v

export(float) var animation_segmentation = 20.0
export(float) var animation_time = 0.01
export (float) var arc_height = 1


func isocoord(a):
	a = Vector2(-a.x + 3, -a.y + 4)
	return Vector2(a[0] + a[1], -0.5 * a[0] + 0.5 * a[1]) * 16 * board_v.scale


func fetch_textures():
	var textures = []
	var cell_size = Vector2(32, 48)
	var canvas_size = atlas.get_size()
	for y in range(canvas_size.y / cell_size.y):
		for x in range(canvas_size.x / cell_size.x):
			var tex = AtlasTexture.new()
			tex.atlas = atlas
			tex.region = Rect2(Vector2(x, y) * cell_size, cell_size)
			textures.append(tex)
	return textures


func gen_piece_sprite(name, textures):
	var code = "-PRNBQKXprnbqkx"
	var relations = {}
	for i in range(-1, 14):
		relations[code[i + 1]] = i
	if relations[name] < 0:
		return false
	var result = Sprite.new()
	result.texture = textures[relations[name]]
	result.offset = Vector2(0, -8)

	# subscribe to piece selected signal
	return result


func animate_movement(from, to, entity):
	var delay = animation_time/animation_segmentation

	var dir = (to - from).normalized()
	var step = (to - from).length()/animation_segmentation

	var current = from + step * dir
	for x in range(animation_segmentation):
		entity.position = isocoord(current)
		yield(get_tree().create_timer(delay), "timeout")
		current += step * dir

func _ready():
	# Subscribe to signals
	board_node.connect("piece_moved", self, "_on_Piece_moved")
	board_node.connect("piece_removed", self, "_on_Piece_removed")
	board_node.connect("piece_placed", self, "_on_Piece_placed")


func initiate():

	board_node = get_parent()
	board_data = board_node.board
	board_v = get_parent().get_node("BoardVisual")
	input_agent = board_node.get_node("InputAgent")
	
	textures = fetch_textures()


	for piece in pieces:
		if not piece == null:
			piece.queue_free()
	pieces.clear()
		

	for y in range(8):
		for x in range(8):
			var sprite = gen_piece_sprite(board_data[y * 8 + x], textures)
			if sprite:
				add_child(sprite)
				sprite.position = isocoord(Vector2(x, y))
				sprite.scale = board_v.scale
				pieces.append(sprite)
			else:
				pieces.append(null)


func _on_Piece_moved(from, to):
	var sprite = pieces[from.y * 8 + from.x]

	# Deal with destroying pieces
	if not pieces[to.y * 8 + to.x] == null:
		pieces[to.y * 8 + to.x].queue_free()

	pieces[to.y * 8 + to.x] = sprite
	pieces[from.y * 8 + from.x] = null

	animate_movement(from, to, sprite)
	sprite.position = isocoord(to)


func _on_Piece_removed(pos, silent):
	pieces[pos.y * 8 + pos.x].queue_free()


func _on_Piece_placed(pos, piece_kind):
	var sprite = gen_piece_sprite(piece_kind, textures)

	# Deal with destroying pieces
	if not pieces[pos.y * 8 + pos.x] == null:
		pieces[pos.y * 8 + pos.x].queue_free()

	sprite.position = isocoord(pos)
	pieces[pos.y * 8 + pos.x] = sprite
	add_child(sprite)
