extends Sprite

onready var board_node = get_parent().get_parent().get_node("Board")

func play_turn():
	print("BOT playing....")
	board_node.game_state = board_node.game_state()
	print(board_node.game_state)
	yield(get_tree().create_timer(0.01), "timeout")
	print("BOT played")
