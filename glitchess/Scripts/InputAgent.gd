extends Node2D

export (Vector2) var cellsize = Vector2(16, 16)
signal board_clicked(board_position)


func cartesian_coord(a):
	return Vector2(a.x * 0.5 - a.y, a.x * 0.5 + a.y)


func calc_board_pos(screen_pos):
	return (-cartesian_coord(screen_pos) / cellsize).floor()


func _input(event):
	if event is InputEventMouseButton and event.pressed:
		var board_position = calc_board_pos(get_local_mouse_position())
		if board_position.x in range(8) and board_position.y in range(8):
			emit_signal("board_clicked", board_position)
