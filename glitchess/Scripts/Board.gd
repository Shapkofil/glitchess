extends Node2D

var init_board = "RNBKQBNRPPPPPPPP--------------------------------pppppppprnbkqbnr"
var board

var index_map = {
	"-": -1,
	"K": 0,
	"X": 1,
	"Q": 2,
	"R": 3,
	"B": 4,
	"N": 5,
	"P": 6,
	"k": 0,
	"x": 1,
	"q": 2,
	"r": 3,
	"b": 4,
	"n": 5,
	"p": 6
}
var pieces = []
var turn = true
var masks = []
var game_state = "None"

var history = []

var used = []

onready var input_agent = get_node("InputAgent")
onready var turn_queue = get_parent().get_node("UI").get_node("TurnQueue")
onready var glitch_data = fetch_glitches()

signal piece_moved(from, to)
signal piece_removed(pos, silent)
signal piece_placed(pos, type)
signal glitched(pos, type)
signal moves_queried(move_set, capture_set)
signal board_initiated
signal visual_reset
signal turn_ended


func simulate(simulation, origin):
	if simulation == null:
		return origin
	return simulation


func load_mask(path):
	var img = Image.new()
	if not img.load(path) == 0:
		print("Maskloading Error:%s" % img.load(path))
	return img


func fetch_glitches():
	var file = File.new()
	file.open("res://glitches.json", File.READ)
	var raw_data = file.get_as_text()
	file.close()
	var result  = JSON.parse(raw_data)
	if result.error:
		print (result.error)
		return {}
	return result.result


func prim_norm(a):
	var x = max(a[0], a[1])
	return a / x


func is_white(query, verbose = false):
	# Empty pass protection
	if query == "-":
		if verbose:
			print("no color")
		return false

	var regex = RegEx.new()
	regex.compile("^[a-z]$")
	if not regex.search(query) == null:
		return false
	return true


func query_pieces(expression, sim_board = null):
	sim_board = simulate(sim_board, board)
	var piece_set = []
	var regex = RegEx.new()
	regex.compile(expression)
	for y in range(8):
		for x in range(8):
			if regex.search(sim_board[y * 8 + x]):
				piece_set.append(Vector2(x, y))
	return piece_set


func move_state(a, origin, sim_board = null, glitched=true):
	sim_board = simulate(sim_board, board)

	if glitched and a.x in range (1,7):
		if search_for_glitch_at(a, simulate_primitive_move(origin, a, sim_board)).size() > 0:
			return "2"

	if sim_board[a.y * 8 + a.x] == "-" :
		return "0"

	if not is_white(sim_board[a.y * 8 + a.x]) == is_white(sim_board[origin.y * 8 + origin.x]):
		return "1"

	return "-"


func apply_mask(a, b, mask, sim_board):
	var mask_coord = (
		(b - a) * ((int(not is_white(sim_board[a.y * 8 + a.x])) - 0.5) * 2)
		+ Vector2(8, 8)
	)
	mask.lock()
	var mask_value = mask.get_pixelv(mask_coord)
	mask.unlock()

	match mask_value:
		Color(0, 0, 0, 1):
			return true
		Color(1, 0, 0, 1):
			if not is_white(sim_board[a.y * 8 + a.x]) == is_white(sim_board[b.y * 8 + b.x]) \
			and not sim_board[b.y*8+b.x] == "-":
				return true

			# en passant
			var dir = 2 * int(is_white(sim_board[a.y * 8 + a.x])) - 1
			if not(b.y == 0 or b.y == 7 or a.y == 7 or a.y == 7) and history.size() > 0:
				if not is_white(sim_board[a.y * 8 + a.x]) == is_white(sim_board[(b.y - dir) * 8 + b.x]) \
				and (sim_board[(b.y - dir) * 8 + b.x] == "p" or sim_board[(b.y - dir) * 8 + b.x] == "P") \
				and history[-1][0] == b + Vector2(0, dir):
					return true

		Color(0, 1, 0, 1):
			if a.y == abs(7*int(not is_white(sim_board[a.y * 8 +a.x])) - 1) \
			and sim_board[b.y * 8 + b.x] == "-":
				return true

		Color(0, 0, 1, 1):
			if sim_board[b.y*8+b.x] == "-":
				return true

	return false


func castling(a, sim_board):
	#ugly castling query
	var white = is_white(sim_board[a.y*8 + a.x])
	var rank = 7*int(not white)
	var move_set = []
	var regex = RegEx.new()
	regex.compile("...[kK]---[Rr]")
	if not regex.search(sim_board.substr(rank*8, 8)) == null:

		var expr = true
		for x in range(3,8):
			expr = expr and not Vector2(x,rank) in side_possible_moves(not white)
			if not expr:
				break
		if expr:
			move_set.append(a - Vector2(-2, 0))

	regex.compile("[rR]--[kK]....")
	if not regex.search(sim_board.substr(rank*8, 8)) == null:
		var expr = true
		for x in range(4):
			expr = expr and not Vector2(x,rank) in side_possible_moves(not white)
			if not expr:
				break
		if expr:
			move_set.append(a - Vector2(2, 0))
	return move_set


func standart_moves(a, sim_board = null, gaem_state = null, verbose = false):
	sim_board = simulate(sim_board, board)
	gaem_state = simulate(gaem_state, game_state)

	var move_set = []
	var capture_set = []
	var glitch_set = []

	var origin_piece = sim_board[a.y * 8 + a.x]

	# line cases
	var mask = masks[index_map[sim_board[a.y * 8 + a.x]]]
	for y in range(-1, 2):
		for x in range(-1, 2):
			var dir = Vector2(x, y)
			if dir == Vector2(0, 0):
				continue
			var curr = a + dir
			while curr.x in range(8) and curr.y in range(8):
				if not apply_mask(a, curr, mask, sim_board):
					break
				match move_state(curr, a, sim_board):
					"-":
						break
					"0":
						move_set.append(curr)
					"1":
						capture_set.append(curr)
						break
					"2":
						glitch_set.append(curr)
				curr += dir

	# jump cases
	var regex = RegEx.new()
	regex.compile("[nxNX]")
	if regex.search(origin_piece):
		for init_x in [-1, 1]:
			for init_y in [-2, 2]:
				var x = a.x + init_x
				var y = a.y + init_y
				if x in range(8) and y in range(8):
					match move_state(Vector2(x, y), a, sim_board):
						"-":
							pass
						"0":
							move_set.append(Vector2(x, y))
						"1":
							capture_set.append(Vector2(x, y))
						"2":
							glitch_set.append(Vector2(x,y))


				x = a.x + init_y
				y = a.y + init_x
				if x in range(8) and y in range(8):
					match move_state(Vector2(x, y), a, sim_board):
						"-":
							pass
						"0":
							move_set.append(Vector2(x, y))
						"1":
							capture_set.append(Vector2(x, y))
						"2":
							glitch_set.append(Vector2(x,y))


	#Castling
	regex.compile("[kK]")
	if regex.search(origin_piece) and gaem_state == "none":
		move_set += castling(a, sim_board)


		# Check elimination
	if is_white(sim_board[a.y * 8 + a.x]) == turn:
		for move_pos in move_set + capture_set:
			if is_in_check(simulate_move(a, move_pos)[0]):
				move_set.erase(move_pos)
				capture_set.erase(move_pos)

	# print the move set
	if verbose:
		print("Standart Moves: ", move_set, "\nStandart Captures:", capture_set)

	return [move_set, capture_set, glitch_set]


func side_possible_moves(is_white, sim_board = null, gaem_state = null):
	sim_board = simulate(sim_board, board)
	gaem_state = simulate(gaem_state, game_state)
	var piece_coords = []
	if is_white:
		piece_coords = query_pieces("[A-Z]", sim_board)
	else:
		piece_coords = query_pieces("[a-z]", sim_board)

	var result = [[], [], []]

	for curr in piece_coords:
		var data = standart_moves(curr, sim_board, gaem_state)
		for i in range(data.size()):
			result[i] += data[i]

	return result


func search_for_glitch_at(pos, sim_board):
	var move_set = []
	var dir = 2 * int(not turn) - 1
	var x = pos.x
	var y = pos.y
	var encoded = sim_board.substr((y+dir)*8 + x - 1, 3) + \
		sim_board.substr((y)*8 + x - 1, 3) + \
		sim_board.substr((y-dir)*8 + x - 1, 3)

	if not pos.x in range(1,7):
		return []

	if not encoded.length() == 9:
		return []

	for glitch in glitch_data:
		var glitch_pattern = glitch
		if not turn:
			glitch_pattern = glitch.to_lower()
		if encoded.match(glitch_pattern):
			for move in glitch_data[glitch]:
				var move_data = [move[0]]
				for attr in move.slice(1,2):
					if typeof(attr) == TYPE_ARRAY:
						move_data.append(pos + Vector2(attr[0], attr[1]) * -dir)
					if typeof(attr) == TYPE_STRING:
						if not turn:
							move_data.append(attr.to_lower())
						else:
							move_data.append(attr)
					if typeof(attr) == TYPE_BOOL:
						move_data.append(attr)
						pass
				move_set.append(move_data)
	return move_set


func glitch_query(sim_board=null, pos=null):
	sim_board = simulate(sim_board, board)

	var x_range = range(1, 7)
	var y_range = range(1, 7)

	if not pos == null:
		x_range = range(pos.x - 1, pos.x + 2)
		y_range = range(pos.y - 1, pos.y + 2)

	var move_set = []

	for x in x_range:
		for y in y_range:
			if not y * 8 + x in range(64):
				continue
			move_set += search_for_glitch_at(Vector2(x,y), sim_board)

	return move_set


func execute_event(sim_board, kind, a, b):
	match kind:
		"piece_moved":
			sim_board[b[1] * 8 + b[0]] = sim_board[a[1] * 8 + a[0]]
			sim_board[a[1] * 8 + a[0]] = "-"
		"piece_removed":
			sim_board[a[1] * 8 + a[0]] = "-"
		"piece_placed":
			sim_board[a[1] * 8 + a[0]] = b

	return sim_board


func simulate_primitive_move(a, b, sim_board):
	sim_board[b[1] * 8 + b[0]] = sim_board[a[1] * 8 + a[0]]
	sim_board[a[1] * 8 + a[0]] = "-"
	return sim_board


func simulate_move(a, b, sim_board = null):
	sim_board = simulate(sim_board, board)
	sim_board[b[1] * 8 + b[0]] = sim_board[a[1] * 8 + a[0]]
	sim_board[a[1] * 8 + a[0]] = "-"


	var dir = 2 * int(is_white(sim_board[b.y * 8 + b.x])) - 1

	# Castling
	var move_seq = [["piece_moved",a,b]]
	var regex = RegEx.new()
	regex.compile("[kK]")
	if not regex.search(sim_board[b.y * 8 + b.x]) == null and a.x == 3 and abs(a.x - b.x) == 2:
		var d = a + Vector2((b.x-a.x)/2, 0)
		var c = Vector2(7*int(not a.x>b.x), a.y)
		sim_board[d.y*8+d.x] = sim_board[c.y*8+c.x]
		sim_board[c.y*8+c.x] = "-"
		move_seq.append(["piece_moved",c,d])

	# En Passant
	regex.compile("[pP]")
	if not (b.y == 0 or b.y == 7):
		if not is_white(sim_board[b.y * 8 + b.x]) == is_white(sim_board[(b.y - dir) * 8 + b.x]) \
		and (not regex.search(sim_board[b.y * 8 + b.x]) == null) \
		and (not regex.search(sim_board[(b.y - dir) * 8 + b.x]) == null) \
		and b.y == abs(7*(dir + 1)/2 - 2) \
		and history[-1][0] == b + Vector2(0, dir) and history[-1][1] == b - Vector2(0, dir):
			sim_board[(b.y - dir) * 8 + b.x] = "-"
			move_seq.append(["piece_removed", b - Vector2(0, dir), true])

	# Promotion
	if sim_board[b.y * 8 + b.x].matchn("P") and \
	b.y == abs(7 * (dir + 1)/2):
		if b.y == 7:
			sim_board[b.y * 8 + b.x] = "X"
			move_seq.append(["piece_placed", b, "X"])
		else:
			sim_board[b.y * 8 + b.x] = "x"
			move_seq.append(["piece_placed", b, "x"])


	# Glitches
	var glitch_move_seq = glitch_query(sim_board, b)
	for move in glitch_move_seq:
		sim_board = execute_event(sim_board, move[0], move[1], move[2])
	move_seq += glitch_move_seq
	if glitch_move_seq.size() > 0:
		move_seq.append(["glitched", true, true])

	return [sim_board, move_seq]


func is_in_check(sim_board = null, gaem_state = null):
	sim_board = simulate(sim_board, board)
	gaem_state = simulate(gaem_state, game_state)
	# Check .... Check XD
	var king_symbol = "k"
	if turn:
		king_symbol = "K"
	var result = (
		query_pieces(king_symbol, sim_board)[0]
		in side_possible_moves(not turn, sim_board, gaem_state)[1]
	)
	return result


func verify_move(a, b, sim_board = null):
	sim_board = simulate(sim_board, board)
	var move_map = standart_moves(a, sim_board)
	return b in move_map[0] or b in move_map[1] or b in move_map[2]


func move(a, b):
	if verify_move(a, b):
		var data = simulate_move(a, b)
		board = data[0]
		for set in data[1]:
			emit_signal(set[0], set[1], set[2])

		# write to history
		history.append([a,b])
		return true
	else:
		print("wrong move")
		return false


func game_state(sim_board = null, gaem_state = null):
	sim_board = simulate(sim_board, board)
	gaem_state = simulate(gaem_state, game_state)
	var state = "none"

	# Destroyed Kings
	var king_symbol = "k"
	if turn:
		king_symbol = "K"
	if query_pieces(king_symbol, sim_board).size() == 0:
		return "glitchmate"

	if is_in_check(sim_board, gaem_state):
		state = "check"

	# Checkmate check
	var move_sets = side_possible_moves(turn, sim_board)
	if move_sets[0].size() == 0 and move_sets[1].size() == 0 and move_sets[2].size() == 0:
		if state == "check":
			state = "checkmate"
		else:
			state = "stalemate"


	return state


func _ready():
	# loading moving masks
	input_agent.connect("board_clicked", self, "_on_Board_Clicked")
	turn_queue.connect("queue_changed", self, "_on_Turn_Queue_Changed")
	for i in range(7):
		masks.append(load_mask("MovementMasks/masks_%s.png" % (i + 1)))


func initiate():
	# initiate board
	board = init_board
	get_node("Pieces").initiate()


var move_cache = null
func _on_Board_Clicked(position):
	var curr_piece = board[position.y * 8 + position.x]
	if not move_cache == null and move(move_cache, position):
		emit_signal("visual_reset")
		move_cache = null
		emit_signal("turn_ended")
	else:
		if not move_cache == null:
			emit_signal("visual_reset")
		if is_white(board[position.y * 8 + position.x]) == turn and \
		not board[position.y * 8 + position.x] == "-":
			move_cache = position
			var move_sets = standart_moves(position)
			emit_signal("moves_queried", move_sets[0], move_sets[1], move_sets[2])


func _on_Turn_Queue_Changed(acid):
	turn = not acid
	game_state = game_state()
	used.clear()
