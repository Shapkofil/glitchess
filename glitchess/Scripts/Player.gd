extends TextureRect


func play_turn():
	self.modulate = Color(1,1,1,1)
	var board_node = get_parent().get_parent().get_parent().get_node("Board")
	yield(board_node,"turn_ended")
	self.modulate = Color(0,0,0,1)
	return 0
