/*
 * MIT License
 * Copyright © Etienne 'Eethe' Orlhac
 * 07/08/2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the “Software”), 
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * The Software is provided “as is”, without warranty of any kind, 
 * express or implied, including but not limited to the warranties of merchantability, 
 * fitness for a particular purpose and noninfringement. In no event shall the authors 
 * or copyright holders be liable for any claim, damages or other liability, whether in an 
 * action of contract, tort or otherwise, arising from, out of or in connection with 
 * the software or the use or other dealings in the Software.
 */
shader_type canvas_item;
uniform float amplitute = .1;
uniform float speed = 5.0;

float rand(in vec2 n) { 
	return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

// replace with noise texture for better performance (probably) - azdcf
vec4 rgbnoise(in vec2 p) {
	return vec4( vec3(
		rand(p+183.234),
		rand(p+32.28),
		rand(p+161.837)
	) * rand(p), 1);
}

vec4 vec4pow( in vec4 v, in float p ) {
    return vec4(pow(v.x,p),pow(v.y,p),pow(v.z,p),v.w); 
}

void fragment() {
	// noise shift
    vec4 shift = vec4pow(rgbnoise(vec2(speed*TIME,2.0*speed*TIME/25.0))*rgbnoise(round(sin(TIME)*SCREEN_UV*32.0)/32.0),4.0)
	*vec4(amplitute,amplitute,amplitute,1.0);
	vec4 c = texture(TEXTURE,UV+shift.xy);
	COLOR = c;
}